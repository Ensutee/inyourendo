package main

import (
    "log"
	"time"
    "net/http"
    "strconv"
    "io/ioutil"
    "strings"
	
	"github.com/googollee/go-socket.io"
)


func main() {
	
    var ip = getHostIp()
	log.Println("server-ip: " + ip)


	var mainClient socketio.Socket = nil

    var idCounter int = 0
	var players = make([]socketio.Socket, 200)
	var playerTimeouts = make([]int, 200)
	
	var cmdCounter int = 0;
	var cmds = make([]string, 2000)

	var numFailPings = 4
	timer := time.Now()
	
	// initialize server
    server, err := socketio.NewServer(nil)
    if err != nil {
        log.Fatal(err)
    }
	
	// handle incoming connection
    server.On("connection", func(so socketio.Socket) {
		if idCounter <= 190 {
			
			//log.Println("on connection")
			so.Join("game")
			

			
			// ----------------------- CONTROLLER ----------
			
			// assign id to connection 
			so.On("id", func(msg string) {
				if mainClient != nil {
					// TODO : check if has main client otherwise will crash
					log.Println("id request: assigning -> " + strconv.Itoa(idCounter))
					players[idCounter] = so
					playerTimeouts[idCounter] = numFailPings
					so.Emit("id", strconv.Itoa(idCounter))
					mainClient.Emit("join", strconv.Itoa(idCounter)) // forward to main client
					
					
					idCounter++
				} else {
					//log.Println("WARNING: mainClient == null -> joinging failed")
				}
				
			})
			
			// add input to input list
			so.On("input", func(msg string) {
				if mainClient != nil{
					//log.Println("player input " + msg)
					cmds[cmdCounter] = msg;
					cmdCounter++;
				}
			})
			
			so.On("ping", func(msg string) {
				// only update ping if actually connected to a game
				if mainClient != nil {
					id, _ := strconv.Atoi(msg)
					if id < idCounter{
						playerTimeouts[id] = numFailPings
						
						elapsed := time.Since(timer)
						if elapsed > 3 {
							timer = time.Now()
							
							
							for i := 0; i < idCounter; i++ {
								playerTimeouts[i]--;
								if playerTimeouts[i] == 0 {
									mainClient.Emit("timeout", strconv.Itoa(i))
								}
							}
						}
					}
				}
			})
			
			
			
			
			// -------------------------- GAME ---------------------
			// "get" sent from unity client only 
			so.On("main", func(msg string) {
				if mainClient == nil{
					//log.Println("main client connected")
					mainClient = so
					so.Emit("main")
					
					// reset connections on zero
					cmdCounter = 0
					idCounter = 0
				}else{
					//log.Println("already got a main")
				}
			})
			
			so.On("bgcolor", func(msg string) {
				msgs := strings.Split(msg, " ")
				
				id, _ := strconv.Atoi(msgs[0])
				//log.Println("assigning new color to ", id)
				
				if players[id] != nil{
					players[id].Emit("color", msgs[1])
				}
			})
			
			// "get" sent from unity client only 
			so.On("get", func(msg string) {
				// TODO : build actual frkn input list
				
				var inputString = ""
				
				// TODO : delete comands
				for i := 0; i < cmdCounter; i++ {
					if i > 0 {
						inputString += " "
					}
					inputString += cmds[i]
				}
				
				cmdCounter = 0
				so.Emit("input", inputString)
			})
			
			// check for disconnect
			so.On("disconnection", func() {
				if so == mainClient{
					//log.Println("main client disconnection")
					for i := 0; i < idCounter; i++ {
						players[i].Emit("end")
					}
					mainClient = nil
				}
			})
			
		} else {
			log.Println("connection refused too many players")
		}
	})
		
	
	
	
	
	
	// ## listen for errors
    server.On("error", func(so socketio.Socket, err error) {
        log.Println("error:", err)
    })
	
	// #### start listening to incoming
    http.Handle("/socket.io/", server)
    http.Handle("/", http.FileServer(http.Dir("./asset")))
    log.Println("Serving at localhost:8080...")
    log.Fatal(http.ListenAndServe(":8080", nil))
}


func getHostIp() string{
    resp, _ := http.Get("http://88.198.161.6/temp/ip.php")
    
    
    defer resp.Body.Close()
    contents, _ := ioutil.ReadAll(resp.Body)
    var ip = string(contents)
    return ip
}