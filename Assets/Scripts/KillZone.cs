﻿using UnityEngine;
using System.Collections;

public class KillZone : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Character") {
            CharacterController character = other.GetComponent<CharacterController>();
            GameManager.RespawnPlayer(character);
        }

        if (other.tag == "Ball")
        {
            GameManager.RespawnBall();
            AudioManager.PlayAudio(AudioManager.SoundType.BallReset, transform, false);
        }
    }
}
