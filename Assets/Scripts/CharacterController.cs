﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterController : MonoBehaviour {
    public const float bumpForce = 30;
    public const float bumpCooldown = .5f;
    public const float idleAudioDelay = 20;

    [Header("Settings")]
    public int id;
    public GameManager.TeamColors team;
    public float acceleration;
    public float friction;

    [Header("References")]
    public PhysicMaterial noFrictionMaterial;
    public PhysicMaterial bounceMaterial;
    public ParticleSystem JumpSystem;
    private Rigidbody rBody;
    private Collider col;

    private bool isBumping;
    private bool collidedWithBall;
    private bool isGrounded;
    float audioTimer;
    float fallTimer;


    // Use this for initialization
    void Start()
    {
        JumpSystem = GetComponentInChildren<ParticleSystem>();
        UpdateColor();
        JumpSystem.gameObject.SetActive(false);
        rBody = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        isBumping = false;
        audioTimer = Random.Range(-idleAudioDelay * .5f, idleAudioDelay * .5f);
        fallTimer = Random.Range(-idleAudioDelay * .5f, idleAudioDelay * .5f);

        //call move once to change the friction of the rigidbody
        Move(Vector3.zero);
    }
    void LateUpdate()
    {
        collidedWithBall = false;
    }

    // Update is called once per frame
    void Update()
    {


        //get input from server
        List<Vector3> movementInputList = new List<Vector3>(InputManager.GetPlayerMovement(id));
        foreach (Vector3 input in movementInputList)
        {
            Move(input);
        }

        List<string> buttonInputList = new List<string>(InputManager.GetPlayerInput(id));
        foreach (string input in buttonInputList)
        {
            Input(input);
        }
        PlayAudio();

        UpdateColor();
    }

    public void UpdateColor() {
        Material mat = GetComponent<Renderer>().material;
        switch (team)
        {
            case GameManager.TeamColors.Red: mat.color = GameManager.instance.redTeamColor; break;
            case GameManager.TeamColors.Blue: mat.color = GameManager.instance.blueTeamColor; break;
            case GameManager.TeamColors.Green: mat.color = GameManager.instance.greenTeamColor; break;
            case GameManager.TeamColors.Yellow: mat.color = GameManager.instance.yellowTeamColor; break;
        }
    }

    private void PlayAudio()
    {
        if (Time.time > audioTimer + idleAudioDelay)
        {
            AudioManager.PlayAudio(AudioManager.SoundType.CharIdle, transform);
            audioTimer = Time.time + Random.Range(-3.5f, 3.5f);
            return;
        }

        if (transform.position.y < 0 && Time.time > fallTimer + idleAudioDelay)
        {
            AudioManager.PlayAudio(AudioManager.SoundType.CharFall, transform, false);
            fallTimer = Time.time;
        }
    }

    public void Move(Vector3 movDir)
    {
        if (isBumping) return;
        Vector3 camForward = Camera.main.transform.forward;
        camForward.y = 0;
        camForward.Normalize();
        movDir = movDir.x * Camera.main.transform.right + movDir.z * camForward;
        rBody.drag = friction;
        rBody.AddForce(movDir.normalized * acceleration, ForceMode.Acceleration);
    }
    public void Input(string command)
    {
        //Debug.Log(command);

        if (command == "bump")
        {
            if (isBumping) return;
            if (!isGrounded) return;
            

            StartCoroutine(BumpRoutine());
        }
    }

    private IEnumerator BumpRoutine()
    {
        //Debug.Log("start bump routine");
        isBumping = true;
        float initialDrag = rBody.drag;

        rBody.drag = 0;
        rBody.velocity = Vector3.zero;
        rBody.angularVelocity = Vector3.zero;
        col.material = bounceMaterial;

        Vector3 ballDir = GameManager.instance.ball.transform.position - transform.position;
        Vector3 forceDir = ballDir + Vector3.up * 3;

        rBody.AddForce(forceDir.normalized * bumpForce, ForceMode.Impulse);
        AudioManager.PlayAudio(AudioManager.SoundType.CharBump, transform);
        JumpSystem.transform.LookAt(-ballDir);
            JumpSystem.gameObject.SetActive(true);
        yield return null;
        float timer = Time.time;
        while (!isGrounded || Time.time < timer + bumpCooldown) 
		{ 
			// Debug.Log("wait"); 
			yield return null; 
		}
        //Debug.Log("back on ground");

        rBody.drag = initialDrag;
        col.material = noFrictionMaterial;

        JumpSystem.gameObject.SetActive(false);
        isBumping = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            if (contact.otherCollider.tag == "Ball")
            {
                //Debug.Log("ball collision");
                collidedWithBall = true;
            }

            if (collision.contacts[0].otherCollider.tag == "Ground")
            {
                //Debug.Log("ground collision");
                isGrounded = true;
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            if (collision.contacts[0].otherCollider.tag == "Ground")
            {
                //Debug.Log("ground collision");
                isGrounded = false;
            }
        }
    }
}
