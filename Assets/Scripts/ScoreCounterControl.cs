﻿using UnityEngine;
using System.Collections;

public class ScoreCounterControl : MonoBehaviour
{
    public GameManager.TeamColors m_Color;
    public Mesh[] Numbers;
    private MeshFilter m_MeshFilter;
    private Animator m_animator;

    // Use this for initialization
    void Start()
    {
        //m_animator = GetComponent<Animator>();
        //m_animator.Play("ScoreIdle", -1, Random.Range(0, 30));
        m_MeshFilter = GetComponent<MeshFilter>();
        m_MeshFilter.mesh = Numbers[0];
    }

    public void ChangeScoreUsingColor(int s)
    {
        m_MeshFilter.mesh = Numbers[s];
    }
}