﻿using UnityEngine;
using System.Collections;

public class CloudMove : MonoBehaviour {
    Vector3 move = Vector3.forward * 5;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
        transform.position += move * Time.deltaTime;

        if(transform.position.z >= 550)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -transform.position.z);
        }
	}
}