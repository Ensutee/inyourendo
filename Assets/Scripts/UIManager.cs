﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {

    [Header("References")]
    public Text redScore;
    public Text blueScore;
    public Text greenScore;
    public Text yellowScore;
	
	// Update is called once per frame
	void Update () {
	    foreach (GameManager.Team team in GameManager.instance.teams)
        {
            switch (team.teamColor)
            {
                case GameManager.TeamColors.Red: redScore.text = team.score.ToString(); break;
                case GameManager.TeamColors.Blue: blueScore.text = team.score.ToString(); break;
                case GameManager.TeamColors.Green: greenScore.text = team.score.ToString(); break;
                case GameManager.TeamColors.Yellow: yellowScore.text = team.score.ToString(); break;
            }
        }
	}
}
