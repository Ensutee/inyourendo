﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public const float ballBaseWeight = 7;
    public const float goalExplosionForce = 75;
    public GameObject Leek;

    public enum TeamColors { Red, Blue, Green, Yellow }
    [HideInInspector] public List<Team> teams;

    private GameObject DroppedLeek = null;

    [Header("Settings")]
    public Color redTeamColor;
    public Color blueTeamColor;
    public Color greenTeamColor;
    public Color yellowTeamColor;

    [Header("References")]
    public Transform ball;
    public Connection connection;

    private Rigidbody ballRigidbody;
    private Vector3 ballInitialPosition;
    public GameObject[] m_CountdownNumbers;

    public class Team
    {
        public TeamColors teamColor;
        public List<CharacterController> players;
        public int score;
        public ScoreCounterControl scoreControl;
        public Team(TeamColors team)
        {
            teamColor = team;
            players = new List<CharacterController>();
            for(int i = 0; i < GameObject.FindGameObjectsWithTag("Score").Length; i++)
            {
                if(GameObject.FindGameObjectsWithTag("Score")[i].GetComponent<ScoreCounterControl>().m_Color == team)
                {
                    scoreControl = GameObject.FindGameObjectsWithTag("Score")[i].GetComponent<ScoreCounterControl>();
                    i = 10;
                }
            }
        }
    }
    public static Team GetTeamByColor(TeamColors color)
    {
        return instance.teams.Find(x => x.teamColor == color);
    }

    void Awake () {
        instance = this;
        teams = new List<Team>();
        teams.Add(new Team(TeamColors.Red));
        teams.Add(new Team(TeamColors.Blue));
        teams.Add(new Team(TeamColors.Green));
        teams.Add(new Team(TeamColors.Yellow));

        ballRigidbody = ball.GetComponent<Rigidbody>();
        ballRigidbody.Sleep();
        ballInitialPosition = ball.position;
        for(int i = 0; i<m_CountdownNumbers.Length; i++)
        {
            m_CountdownNumbers[i].SetActive(false);
        }
    }

    void Update()
    {
        UpdateBallWeight();
        PlayBallRollingSounds();

        //if (Input.GetKeyDown(KeyCode.Space)) AddPlayer(0);
    }

    public void StartGame()
    {
        if(DroppedLeek != null)
        {
            Destroy(DroppedLeek);
            DroppedLeek = null;
        }
        ResetScore();
        StartCoroutine(Countdown());
    }

    IEnumerator Countdown()
    {
        //3
        m_CountdownNumbers[0].SetActive(true);
        SCREENSHAKE.SHAKE(0.05f, 1.5f);
        yield return new WaitForSeconds(1);
        //2
        SCREENSHAKE.SHAKE(0.05f, 1.5f);
        m_CountdownNumbers[0].SetActive(false);
        m_CountdownNumbers[1].SetActive(true);
        yield return new WaitForSeconds(1);
        //1
        SCREENSHAKE.SHAKE(0.05f, 1.5f);
        m_CountdownNumbers[1].SetActive(false);
        m_CountdownNumbers[2].SetActive(true);
        yield return new WaitForSeconds(1);
        SCREENSHAKE.SHAKE(0.05f, 1.5f);
        m_CountdownNumbers[2].SetActive(false);
        ballRigidbody.WakeUp();
    }

    //adjust the ball weight by the amount of people on biggest team
    private void UpdateBallWeight()
    {
        teams.Sort((x, y) => x.players.Count.CompareTo(y.players.Count));
        ballRigidbody.mass = teams[3].players.Count * ballBaseWeight;
    }

    float timer = -Mathf.Infinity;
    private void PlayBallRollingSounds()
    {
        if (Time.time > timer + 20 && ball.GetComponent<Rigidbody>().velocity.magnitude > 1)
        {
            AudioManager.PlayAudio(AudioManager.SoundType.BallRoll, ball);
            timer = Time.time;
        }
    }

    public static void AddPlayer(int id) { instance.InstantiateCharacter(id); }
    public void InstantiateCharacter(int id)
    {
        Debug.Log("Instantiating player " + id.ToString());
        InputManager.AddPlayer(id);

        //sort the teams by player count and pick the lowest populated team
        teams.Sort((x, y) => x.players.Count.CompareTo(y.players.Count));
        Team selectedTeam = teams[0];
        connection.SetControllerColor(id, selectedTeam.teamColor);

        //get spawn position
        Vector3 spawnPoint = SpawnPoint.GetSpawnPosition(selectedTeam.teamColor);

        //instantiate character and assign id
        CharacterController character = CharacterPool.SpawnCharacter(spawnPoint, Quaternion.identity).GetComponent<CharacterController>();
        
        character.id = id;
        character.team = selectedTeam.teamColor;
        selectedTeam.players.Add(character);
    }
    public static void RemovePlayer(int id) { instance.DestroyCharacter(id); }
    public void DestroyCharacter(int id)
    {
        //locate character
        CharacterController character = null;
        foreach (Team team in teams) {
            character = team.players.Find(x => x.id == id);
            if (character != null) break;
        }

        if (character != null) CharacterPool.DespawnCharacter(character.transform);
        else Debug.Log("No character found with id: " + id);
    }

    void ResetScore()
    {
        for(int i = 0; i < teams.Count; i++)
        {
            teams[i].score = 0;
            teams[i].scoreControl.ChangeScoreUsingColor(teams[i].score = 0);
        }
    }

    public static void Goal(TeamColors team) { instance.TeamScored(team); }
    public void TeamScored(TeamColors team)
    {
        Team scoreTeam = teams.Find(x => x.teamColor == team);
        scoreTeam.score += 1;
        scoreTeam.scoreControl.ChangeScoreUsingColor(scoreTeam.score);
        SCREENSHAKE.SHAKE(0.5f, 2);
        AudioManager.PlayAudio(AudioManager.SoundType.Goal, ball.transform, false);
        foreach (Team tm in teams)
        {
            foreach (CharacterController character in tm.players)
            {
                Vector3 forceDir = character.transform.position - ball.transform.position;
                float distanceModifier = 1 - Mathf.InverseLerp(0, 100, forceDir.magnitude);
                character.GetComponent<Rigidbody>().AddForce((forceDir.normalized + Vector3.up) * goalExplosionForce * distanceModifier, ForceMode.Impulse);
            }
        }

        RespawnBall();

        for (int i = 0; i < teams.Count; i++)
        {
            if(teams[i].score >= 3)
            {
                GameFinished();
            }
        }
    }

    void GameFinished()
    {
        DroppedLeek = (GameObject)Instantiate(Leek, new Vector3(0, 75, 0), Quaternion.identity);
        ballRigidbody.Sleep();
    }

    public static void RespawnBall() { instance.ResetBallPosition(); }
    public void ResetBallPosition()
    {
        ball.position = ballInitialPosition;
        Rigidbody rBody = ball.GetComponent<Rigidbody>();

        rBody.velocity = Vector3.zero;
        rBody.angularVelocity = Vector3.zero;
    }

    public static void RespawnPlayer(CharacterController character) { instance.ResetPlayerPosition(character); }
    public void ResetPlayerPosition(CharacterController character)
    {
        Vector3 spawnPoint = SpawnPoint.GetSpawnPosition(character.team);
        character.transform.position = spawnPoint;
        character.transform.rotation = Quaternion.identity;

        Rigidbody rBody = character.GetComponent<Rigidbody>();
        rBody.velocity = Vector3.zero;
        rBody.angularVelocity = Vector3.zero;
    }
}