﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterPool : MonoBehaviour {

    public static CharacterPool instance;

    public GameObject characterPrefab;

    private List<Transform> instantiatedPlayers;

    void Awake()
    {
        instance = this;
        instantiatedPlayers = new List<Transform>();
    }

    public static Transform SpawnCharacter(Vector3 position, Quaternion rotation) { return instance.spawnCharacter(position, rotation); }
	private Transform spawnCharacter(Vector3 position, Quaternion rotation)
    {
        Transform character = instantiatedPlayers.Find(x => !x.gameObject.activeSelf);
        if (character == null)
        {
            character = Instantiate(characterPrefab).transform;
            instantiatedPlayers.Add(character);
        }

        character.parent = transform;
        character.position = position;
        character.rotation = rotation;

        Rigidbody rBody = character.GetComponent<Rigidbody>();
        if (rBody != null)
        {
            rBody.velocity = Vector3.zero;
            rBody.angularVelocity = Vector3.zero;
        }

        character.gameObject.SetActive(true);

        return character;
    }

    public static void DespawnCharacter(Transform character) { instance.despawnCharacter(character); }
    private void despawnCharacter(Transform character)
    {
        character.gameObject.SetActive(false);
    }
}
