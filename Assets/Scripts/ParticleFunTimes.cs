﻿using UnityEngine;
using System.Collections;

public class ParticleFunTimes : MonoBehaviour {
    public GameObject m_BallGroundHit;
    private Color m_Blue;
    private Color m_Red;
    private Color m_Yellow;
    private Color m_Green;
    public Transform m_FaceRotator;
    public Transform m_BubbleRotator;
    public Transform m_Face;

    private Rigidbody m_rBody;
    private float counter = 0.0f;
    private bool emoting = false;
    private bool falling = false;
    private int hitCounter = 0;
    private float timer = 0.0f;
    public GameObject m_ThoughtBubble1;
    public GameObject m_ThoughtBubble2;

    public Material m_FaceMat;
    public Texture NormalFace;
    public Texture[] HappyFaces;
    public Texture[] WorriedFaces;
    public Texture[] UnimpressedFaces;
    public bool emoteReady = true;

    public enum BallEmotion{
        Normal = 0, Happy = 1, Worried = 2, Misc = 3, Unimpressed = 4
    };

    // Use this for initialization
    void Start () {
        GameManager gm = GameManager.instance;
        m_Blue = gm.blueTeamColor;
        m_Red = gm.redTeamColor;
        m_Yellow = gm.yellowTeamColor;
        m_Green = gm.greenTeamColor;
        m_rBody = GetComponent<Rigidbody>();
        m_FaceMat.SetTexture("_MainTex", NormalFace);
        m_ThoughtBubble1.SetActive(false);
        m_ThoughtBubble2.SetActive(false);
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Ground")
        {
            SCREENSHAKE.SHAKE(0.1f, 3f);
            AudioManager.PlayAudio(AudioManager.SoundType.BallDrop, transform, false);
            Instantiate(m_BallGroundHit, other.contacts[0].point, Quaternion.Euler(270,0,0));
        }
        if(other.gameObject.tag == "Character")
        {
            SCREENSHAKE.SHAKE(0.1f, 2f);
            AudioManager.PlayAudio(AudioManager.SoundType.BallSlap, transform, false);
            hitCounter++;
        }
        if(hitCounter >= 5)
        {
            m_ThoughtBubble1.SetActive(true);
            ExpressYourself(BallEmotion.Worried);
            hitCounter = 0;
        }
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (emoteReady)
        {
            if (transform.position.x >= 15 || transform.position.x <= -15 ||
                transform.position.z >= 15 || transform.position.z <= -15)
            {
                ExpressYourself(BallEmotion.Happy);
                m_ThoughtBubble2.SetActive(true);
            }

            
            if (timer >= 4f)
            {
                ExpressYourself(BallEmotion.Unimpressed);
                timer = 0;
            }
        }
        else if(timer >= 4f)
        {
            emoteReady = true;
            timer = 0;
        }

        if(transform.position.y < 3f)
        {
            if (!falling)
            {
                AudioManager.PlayAudio(AudioManager.SoundType.BallFall, transform);
                ExpressYourself(BallEmotion.Worried);
                falling = true;
            }
        }
        else
        {
            if (falling)
            {
                falling = false;
            }
        }

        m_FaceRotator.rotation = Quaternion.Lerp(m_FaceRotator.rotation, Quaternion.LookRotation(m_FaceRotator.position-(Camera.main.transform.position-Vector3.up*15)), Time.deltaTime * 3f);
        m_BubbleRotator.LookAt(Camera.main.transform);

        if (emoting)
        {
            counter += Time.deltaTime;
            if(counter >= 0.75)
            {
                counter = 0;
                emoting = false;
                m_FaceMat.SetTexture("_MainTex", NormalFace);
                m_ThoughtBubble1.SetActive(false);
                m_ThoughtBubble2.SetActive(false);
            }
        }
    }

    void ExpressYourself(BallEmotion emotion)
    {
        counter = 0;
        emoting = true;
        emoteReady = false;
        switch (emotion)
        {
            case BallEmotion.Happy:
                int hf = (int)(Random.Range(0, HappyFaces.Length) + 0.5f);
                m_FaceMat.SetTexture("_MainTex", HappyFaces[hf]);
                break;
            case BallEmotion.Worried:
                int wf = (int)(Random.Range(0, WorriedFaces.Length) + 0.5f);
                m_FaceMat.SetTexture("_MainTex", WorriedFaces[wf]);
                break;
            case BallEmotion.Unimpressed:
                int uf = (int)(Random.Range(0, UnimpressedFaces.Length) + 0.5f);
                m_FaceMat.SetTexture("_MainTex", UnimpressedFaces[uf]);
                break;
            default:
                break;
        }
        
    }
}