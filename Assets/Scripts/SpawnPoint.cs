﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPoint : MonoBehaviour {

    private static List<SpawnPoint> spawnPoints;

    public GameManager.TeamColors teamSpawn;
    private Color teamColor;

    void Awake()
    {
        if (spawnPoints == null) spawnPoints = new List<SpawnPoint>();
        spawnPoints.Add(this);
    }

    public static Vector3 GetSpawnPosition(GameManager.TeamColors team)
    {
        List<SpawnPoint> teamSpawns = spawnPoints.FindAll(x => x.teamSpawn == team);
        SpawnPoint spawn = teamSpawns[Random.Range(0, teamSpawns.Count)];

        Vector3 spawnPosition = GetRandomPositionWithinTransform(spawn.transform);

        return spawnPosition;
    }
    private static Vector3 GetRandomPositionWithinTransform(Transform transform)
    {
        float xPos = Random.Range(
            transform.position.x - (transform.localScale.x * .5f),
            transform.position.x + (transform.localScale.x * .5f)
            );

        float zPos = Random.Range(
            transform.position.z - (transform.localScale.z * .5f),
            transform.position.z + (transform.localScale.z * .5f)
            );

        return new Vector3(xPos, transform.position.y, zPos);
    }

    void OnDrawGizmosSelected()
    {
        //set team color
        switch (teamSpawn)
        {
            case GameManager.TeamColors.Red: teamColor = Color.red; break;
            case GameManager.TeamColors.Blue: teamColor = Color.blue; break;
            case GameManager.TeamColors.Green: teamColor = Color.green; break;
            case GameManager.TeamColors.Yellow: teamColor = Color.yellow; break;
        }

        Gizmos.color = teamColor;
        Gizmos.DrawCube(transform.position, transform.localScale);
    }
}
