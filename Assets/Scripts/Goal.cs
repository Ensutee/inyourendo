﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {

    public GameManager.TeamColors teamGoal;
    
    void OnTriggerEnter()
    {
        GameManager.Goal(teamGoal);
    }
}
