﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour {

    public static InputManager instance;

    public float msDelay;
    private List<Vector3> p1MovementInput;
    private List<string> p1ButtonInput;

    private List<Vector3> p2MovementInput;
    private List<string> p2ButtonInput;

    private List<Vector3> p3MovementInput;
    private List<string> p3ButtonInput;

    private List<Vector3> p4MovementInput;
    private List<string> p4ButtonInput;

    private List<List<Vector3>> playerMovements;
    private List<List<string>> playerButtons;

    private Vector3 inputDir;

    // Use this for initialization
    void Awake () {
        instance = this;

        playerMovements = new List<List<Vector3>>();
        playerButtons = new List<List<string>>();

        p1MovementInput = new List<Vector3>();
        p1ButtonInput = new List<string>();

        p2MovementInput = new List<Vector3>();
        p2ButtonInput = new List<string>();

        p3MovementInput = new List<Vector3>();
        p3ButtonInput = new List<string>();

        p4MovementInput = new List<Vector3>();
        p4ButtonInput = new List<string>();
    }
	
	// Update is called once per frame
	void Update () {
        /*p1MovementInput.Add(new Vector3(Input.GetAxis("p1Horizontal"), 0, Input.GetAxis("p1Vertical")));
        p1ButtonInput.Add(Input.GetButton("p1Bump") ? "bump" : null);

        p2MovementInput.Add(new Vector3(Input.GetAxis("p2Horizontal"), 0, Input.GetAxis("p2Vertical")));
        p2ButtonInput.Add(Input.GetButton("p2Bump") ? "bump" : null);

        p3MovementInput.Add(new Vector3(Input.GetAxis("p3Horizontal"), 0, Input.GetAxis("p3Vertical")));
        p3ButtonInput.Add(Input.GetButton("p3Bump") ? "bump" : null);

        p4MovementInput.Add(new Vector3(Input.GetAxis("p4Horizontal"), 0, Input.GetAxis("p4Vertical")));
        p4ButtonInput.Add(Input.GetButton("p4Bump") ? "bump" : null);*/
    }

    public static Vector3[] GetPlayerMovement(int id)
    {
        var moveArray = GetMove(id).ToArray();
        ClearMove(id);
        return moveArray;

        /*
        List<Vector3> inputList = instance.p1MovementInput;
        if (id == "1") inputList = instance.p1MovementInput;
        else if (id == "2") inputList = instance.p2MovementInput;
        else if (id == "3") inputList = instance.p3MovementInput;
        else if (id == "4") inputList = instance.p4MovementInput;

        List<Vector3> inputToReturn = new List<Vector3>();
        if (inputList.Count == 0) return inputList.ToArray();
        while (inputList.Count * Time.deltaTime > instance.msDelay)
        {
            inputToReturn.Add(inputList[0]);
            inputList.RemoveAt(0);
        }

        return inputToReturn.ToArray();*/
    }

    public static string[] GetPlayerInput(int id)
    {
        var btnArray = GetButton(id).ToArray();
        ClearButtons(id);
        return btnArray;

        /*
        List<string> inputList = instance.p1ButtonInput;
        if (id == "1") inputList = instance.p1ButtonInput;
        else if (id == "2") inputList = instance.p2ButtonInput;
        else if (id == "3") inputList = instance.p3ButtonInput;
        else if (id == "4") inputList = instance.p4ButtonInput;

        List<string> inputToReturn = new List<string>();
        if (inputList.Count == 0) return inputList.ToArray();
        while (inputList.Count * Time.deltaTime > instance.msDelay)
        {
            inputToReturn.Add(inputList[0]);
            inputList.RemoveAt(0);
        }

        return inputToReturn.ToArray();*/
    }

    public static void AddPlayer(int id)
    {
        if (instance == null) return;
        instance.playerMovements.Add(new List<Vector3>());
        instance.playerButtons.Add(new List<string>());
    }

    public static void AddMovement(int id, Vector3 dir)
    {
        if (instance.playerMovements.Count <= id) return;
        instance.playerMovements[id].Add(dir);
    }
    public static List<Vector3> GetMove(int id)
    {
        return instance.playerMovements[id];
    }
    public static void ClearMove(int id)
    {
        instance.playerMovements[id] = new List<Vector3>();
    }

    public static void AddButton(int id, string btn)
    {
        if (instance.playerButtons.Count <= id) return;
        instance.playerButtons[id].Add(btn);
    }
    public static List<string> GetButton(int id)
    {
        return instance.playerButtons[id];
    }
    public static void ClearButtons(int id)
    {
        instance.playerButtons[id] = new List<string>();
    }
}
