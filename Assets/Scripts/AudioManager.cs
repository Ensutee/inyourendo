﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;

    public enum SoundType
    {
        CharIdle,
        CharFall,
        CharBump,
        CharWon,
        CharLost,
        BallFall,
        BallRoll,
        BallSlap,
        BallDrop,
        BallReset,
        Goal
    }

    [Header("AudioClips")]
    public AudioGroup charIdle;
    public AudioGroup charFall;
    public AudioGroup charBump;
    public AudioGroup charWon;
    public AudioGroup charLost;
    public AudioGroup ballRoll;
    public AudioGroup ballFall;
    public AudioGroup ballDrop;
    public AudioGroup ballSlap;
    public AudioGroup ballReset;
    public AudioGroup goal;

    [Header("References")]
    public GameObject audioSourcePrefab;
    private List<AudioSource> instantiatedAudioSources;

    [System.Serializable]
    public class AudioGroup
    {
        public float volume;
        public Vector2 pitchRange;
        public List<AudioClip> clips;

        public AudioClip GetClip()
        {
            return clips[Random.Range(0, clips.Count)];
        }
    }

    void Awake()
    {
        instance = this;
        instantiatedAudioSources = new List<AudioSource>();
    }

    //=== POOL ===
    private AudioSource SpawnSource()
    {
        AudioSource source = instantiatedAudioSources.Find(x => !x.isPlaying);
        if (source == null)
        {
            GameObject go = Instantiate(audioSourcePrefab);
            source = go.GetComponent<AudioSource>();
            instance.instantiatedAudioSources.Add(source);
        }

        return source;
    }
    //=== END POOL ===

    private void PlayClip(AudioGroup group, Transform parent, bool follow)
    {
        AudioSource source = SpawnSource();
        source.clip = group.GetClip();
        source.playOnAwake = false;
        source.loop = false;

        source.volume = group.volume;
        source.pitch = Random.Range(group.pitchRange.x, group.pitchRange.y);

        source.transform.position = parent.transform.position;
        if (follow) source.transform.parent = parent;

        source.Play();
    }

    public static void PlayAudio(SoundType type, Transform parent, bool follow = true) { instance.HandleAudioType(type, parent, follow); }
    private void HandleAudioType(SoundType type, Transform parent, bool follow)
    {
        switch (type)
        {
            case SoundType.CharIdle:
                PlayClip(charIdle, parent, follow);
                break;

            case SoundType.CharFall:
                PlayClip(charFall, parent, follow);
                break;

            case SoundType.CharBump:
                PlayClip(charBump, parent, follow);
                break;

            case SoundType.CharLost:
                PlayClip(charLost, parent, follow);
                break;

            case SoundType.CharWon:
                PlayClip(charWon, parent, follow);
                break;

            case SoundType.BallRoll:
                PlayClip(ballRoll, parent, follow);
                break;

            case SoundType.BallFall:
                PlayClip(ballFall, parent, follow);
                break;

            case SoundType.BallDrop:
                PlayClip(ballDrop, parent, follow);
                break;

            case SoundType.BallSlap:
                PlayClip(ballSlap, parent, follow);
                break;

            case SoundType.BallReset:
                PlayClip(ballReset, parent, follow);
                break;

            case SoundType.Goal:
                PlayClip(goal, parent, follow);
                break;
        }
    }
}