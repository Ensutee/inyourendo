﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SCREENSHAKE : MonoBehaviour {
    
    public static List<SCREENSHAKE> cameras;

    private Vector3 m_StartPos = Vector3.zero;
    private Vector3 m_CurrentPos = Vector3.zero;
    private Vector3 m_StartRot = Vector3.zero;
    public float m_ShakeDuration = 0.0f;
    public float m_ShakeIntensity = 0.0f;
    private float m_Counter = 0.0f;
    public bool m_SHAKEING = false;

    // Use this for initialization
    void Start() {
        m_StartPos = transform.position;
        m_CurrentPos = m_StartPos;
        m_StartRot = transform.eulerAngles;

        if (cameras == null) cameras = new List<SCREENSHAKE>();
        cameras.Add(this);
    }

    void Update()
    {
        m_CurrentPos = m_StartPos + transform.up * (Mathf.Sin(Time.realtimeSinceStartup) * 0.6f);

        if (m_SHAKEING)
        {
            transform.position = m_CurrentPos + (Random.insideUnitSphere * m_ShakeIntensity);
            m_Counter += Time.deltaTime;
            if(m_Counter >= m_ShakeDuration)
            {
                m_SHAKEING = false;
                m_ShakeDuration = 0.0f;
                m_ShakeIntensity = 0.0f;
                m_Counter = 0.0f;
                transform.position = m_CurrentPos;
            }
        }
        else
        {
            transform.position = m_CurrentPos;
        }
    }

    public static void SHAKE(float duration, float intensity){
        foreach (SCREENSHAKE shake in cameras)
        {
            shake.m_ShakeDuration = duration;
            shake.m_ShakeIntensity = intensity;
            shake.m_SHAKEING = true;
        }
    }
}