﻿using UnityEngine;
using System.Collections;
using SocketIO;
using System;
using System.Collections.Generic;

public class Connection : MonoBehaviour {


    SocketIOComponent socketio;
    Vector3[] moveCmdMap = {
       new Vector3(0,0,1),          
       new Vector3(0,0,-1),         
       new Vector3(-1,0,0),         
       new Vector3(1,0,0)
   };

    List<int> queuedIds = new List<int>();
    List<int> queuedRemoves = new List<int>();
    List<int> playerLastInput = new List<int>();

    bool connected = false;

    public bool debugging = false;

	// Use this for initialization
	void Start () {

        socketio = GetComponent<SocketIOComponent>();
        socketio.hackedConnection = this;
	}

	// Update is called once per frame
	void Update () {
        //  ---------- add player to the game that joined the server
        if (queuedIds.Count > 0)
        {
            foreach (var id in queuedIds)
            {
                GameManager.AddPlayer(id);
            }
            queuedIds = new List<int>();
        }
        if (queuedRemoves.Count > 0)
        {
            foreach (var id in queuedRemoves)
            {
                GameManager.RemovePlayer(id);
            }
            queuedRemoves = new List<int>();
        }


        // ---------- server connection loop ---------
        if (!connected)
        {
            socketio.Emit("main");
        }
        else
        {
            socketio.Emit("get");
        }

        //Debug.Log("count - " + playerLastInput.Count);
        // ----------- input extrapolation ----------
        for(int i = 0; i < playerLastInput.Count; i++)
        {
            //Debug.Log("Input - " + playerLastInput[i]);
            // only extrapolate movement since you keep it pressed
            if (playerLastInput[i] >= 0 && playerLastInput[i] <= 3)
            {
                InputManager.AddMovement(i, moveCmdMap[playerLastInput[i]]);
            }
        }
	}

    public void SetControllerColor(int id, GameManager.TeamColors team)
    {
        var obj = JSONObject.StringObject(id.ToString() + " " + ((int)team).ToString());
        socketio.Emit("bgcolor", obj);
    }

    public void OnPacket(Packet packet)
    {
        // get msg type
        var msgType = packet.json.list[0].ToString();
        if (msgType.Length <= 2) return; // only proceed if commands were queued
        msgType = msgType.Substring(1, msgType.Length - 2); // remove initial string indicators


        if(msgType == "main")
        {
            if (connected) return;
            connected = true;
            Debug.Log("registered as main client");
        }

        else if (msgType == "join")
        {
            Debug.Log("joining..");
            string msg = packet.json.list[1].ToString();
            if (msg.Length <= 2)
            {
                Debug.Log("join failed: " + packet.json);
                return; // only proceed if commands were queued
            }
            msg = msg.Substring(1, msg.Length - 2); // remove initial string indicators
            var id = System.Int32.Parse(msg);

            queuedIds.Add(id);
            playerLastInput.Add(-1);
            Debug.Log("queuing player id: " + id);
        }

        else if (msgType == "input")
        {
            string msg = packet.json.list[1].ToString();
            if (msg.Length <= 4) return; // only proceed if commands were queued
            msg = msg.Substring(1, msg.Length - 2); // remove initial string indicators

            var splits = msg.Split(' ');
            int id = -1;
            int cmd = -1;

            for (var i = 0; i < splits.Length; i += 2)
            {
                id = System.Int32.Parse(splits[i]);
                cmd = System.Int32.Parse(splits[i + 1]);

                // movement cmds 0,1,2,3 = up,down,left,right
                if (cmd >= 0 && cmd <= 3)
                {
                    Vector3 dir = moveCmdMap[cmd];
                    if(!debugging) InputManager.AddMovement(id, dir);
                    if (debugging) Debug.Log("@" + id.ToString() + " dir: " + dir);
                }
                else if (cmd == 4)
                {
                    InputManager.AddButton(id, "bump");
                }

                playerLastInput[id] = cmd; // assign the last cmd to the player
                //Debug.Log("player " + id + " last: " + cmd); 
            }


        }
        else if (msgType == "timeout")
        {

            string msg = packet.json.list[1].ToString();
            if (msg.Length <= 2) return; // only proceed if commands were queued
            msg = msg.Substring(1, msg.Length - 2); // remove initial string indicators
            var id = System.Int32.Parse(msg);
            queuedRemoves.Add(id);
        }

    }
}
