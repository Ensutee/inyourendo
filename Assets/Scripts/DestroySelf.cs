﻿using UnityEngine;
using System.Collections;

public class DestroySelf : MonoBehaviour {
    public float m_TimeToDeath = 0.0f;
    private float counter = 0.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        counter += Time.deltaTime;
        if(counter >= m_TimeToDeath)
        {
            Destroy(this.gameObject);
        }
	}
}
