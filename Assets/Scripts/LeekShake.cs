﻿using UnityEngine;
using System.Collections;

public class LeekShake : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ground")
        {
            SCREENSHAKE.SHAKE(0.5f, 5f);
            AudioManager.PlayAudio(AudioManager.SoundType.BallDrop, transform, false);
        }
    }
}